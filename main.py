import pickle
import os
from sklearn.model_selection import train_test_split


def read_data(label_ids: list):
    all_desired_data = []
    all_desired_label = []

    for file in os.listdir('data'):
        file_name = "data/" + file
        if file == "batches.meta":
            continue
        with open(file_name, 'rb') as fo:
            dict = pickle.load(fo, encoding='bytes')

        desired_data_ids = [i for i, v in enumerate(dict[b'labels']) if v in label_ids]
        desired_data = [dict[b'data'][x] for x in desired_data_ids]
        desired_label = [dict[b'labels'][x] for x in desired_data_ids]
        all_desired_data += desired_data
        all_desired_label += desired_label

    return all_desired_data, all_desired_label


def split_data(x, y):
    x_train_validate, x_test, y_train_validate, y_test = train_test_split(x, y, test_size=0.15, random_state=0, stratify=y)
    x_train, x_validate, y_train, y_validate = train_test_split(x_train_validate, y_train_validate, test_size=(15*len(x))/(100*len(x_train_validate)), random_state=0, stratify=y_train_validate)
    return [x_train, y_train], [x_test, y_test], [x_validate, y_validate]


if __name__ == '__main__':
    label_ids = [4, 7]
    data, label = read_data(label_ids=label_ids)
    train, test, validate = split_data(data, label)
    a = 0
